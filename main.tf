terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.5.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.1.2"
    }
  }
}

provider "aws" {
  region = var.region
}


module "network" {
  for_each = var.environment

  source = "./modules/network"

  project_prefix     = var.project_prefix
  environment_prefix = each.key
  environment        = each.value

  availability_zone_names = var.availability_zone_names[each.key]
  vpc_cidr                = var.vpc_cidr
  public_subnets          = var.public_subnets
  private_ecs_subnets     = var.private_ecs_subnets
  private_db_subnets      = var.private_db_subnets
  db_engine               = var.db_engine
  key_name                = var.key_name[each.key]

  nat_instance_type     = var.nat_instance_type[each.key]
  bastion_instance_type = var.bastion_instance_type[each.key]
}

module "database" {
  for_each = var.environment

  source = "./modules/database"

  project_prefix     = var.project_prefix
  environment_prefix = each.key
  environment        = each.value

  vpc_id         = module.network[each.key].vpc_id
  vpc_cidr_block = module.network[each.key].vpc_cidr_block
  subnet_ids     = module.network[each.key].private_db_subnets_id

  db_engine         = var.db_engine
  db_engine_version = var.db_engine_version

  db_storage_size = var.db_storage_size[each.key]

  db_name     = var.db_name
  db_username = var.db_username

  db_availability_zone = var.db_availability_zone[each.key]

  db_skip_final_snapshot = var.db_skip_final_snapshot
  db_instance_type       = var.db_instance_type[each.key]
}

module "redis" {
  for_each = var.environment

  source = "./modules/redis"

  project_prefix     = var.project_prefix
  environment_prefix = each.key
  environment        = each.value

  vpc_id         = module.network[each.key].vpc_id
  vpc_cidr_block = module.network[each.key].vpc_cidr_block
  subnet_ids     = module.network[each.key].private_db_subnets_id

  instance_type  = var.redis_instance_type[each.key]
  engine_version = var.redis_engine_version
  availability_zone = var.db_availability_zone[each.key]
}

module "elasticsearch" {
  for_each = var.environment

  source = "./modules/elasticsearch"

  project_prefix     = var.project_prefix
  environment_prefix = each.key
  environment        = each.value

  vpc_id         = module.network[each.key].vpc_id
  vpc_cidr_block = module.network[each.key].vpc_cidr_block
  subnet_ids     = module.network[each.key].private_db_subnets_id[0]

  instance_type         = var.elasticsearch_instance_type[each.key]
  volume_size           = var.elasticsearch_storage_size[each.key]
  elasticsearch_version = var.elasticsearch_version
  username              = var.redis_username

  availability_zone = var.db_availability_zone[each.key]
}


module "s3" {
  for_each = var.environment

  source = "./modules/s3"

  project_prefix     = var.project_prefix
  environment_prefix = each.key
  environment        = each.value
}


module "domain" {
  for_each = var.environment

  source = "./modules/domain"

  region      = var.region
  domain_name = var.domain_name[each.key]
  environment = each.value
}

module "alb" {
  for_each = var.environment

  source = "./modules/alb"

  project_prefix     = var.project_prefix
  environment_prefix = each.key
  environment        = each.value

  region          = var.region

  domain_name     = var.domain_name[each.key]
  domain_zone_id  = module.domain[each.key].zone_id
  certificate_arn = module.domain[each.key].certificate_arn

  vpc_id            = module.network[each.key].vpc_id
  public_subnet_ids = module.network[each.key].public_subnets_id
}

module "ecs" {
  for_each = var.environment

  source = "./modules/ecs"

  region             = var.region
  domain_name        = var.domain_name[each.key]
  environment        = each.value
  environment_prefix = each.key
  project_prefix     = var.project_prefix

  vpc_id     = module.network[each.key].vpc_id
  subnet_ids = module.network[each.key].private_ecs_subnets_id

  laravel_image = "019115857347.dkr.ecr.eu-central-1.amazonaws.com/laravel"
  nginx_image   = "019115857347.dkr.ecr.eu-central-1.amazonaws.com/laravel/nginx"


  alb_target_group_arn = module.alb[each.key].alb_target_group_arn

  s3_arn = module.s3[each.key].s3_arn
}
