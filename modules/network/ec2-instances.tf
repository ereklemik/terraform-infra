data "aws_ami" "latest_amzn2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-*-hvm-*-x86_64-gp2"]
  }
}

resource "aws_instance" "nat" {
  count = length(var.availability_zone_names)

  ami           = data.aws_ami.latest_amzn2.id
  instance_type = var.nat_instance_type

  subnet_id                   = aws_subnet.public[count.index].id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.nat.id]

  key_name          = var.key_name
  source_dest_check = false

  user_data = <<-EOF
    #!/bin/bash
    sysctl -w net.ipv4.ip_forward=1
    /sbin/iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
    yum install -y iptables-services
    service iptables save
    EOF
  tags = {
    Name        = "${var.environment_prefix}-nat-instance-${var.availability_zone_names[count.index]}"
    Environment = var.environment
  }
}

resource "aws_instance" "bastion" {
  ami           = data.aws_ami.latest_amzn2.id
  instance_type = var.bastion_instance_type

  key_name = var.key_name

  subnet_id                   = aws_subnet.public[0].id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.bastion.id]
  tags = {
    Name        = "${var.environment_prefix}-bastion"
    Environment = var.environment
  }
}