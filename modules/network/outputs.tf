output "bastion_public_ip" {
  value       = aws_instance.bastion.public_ip
  description = "Bastion host public IP"
}

output "bastion_public_dns" {
  value       = aws_instance.bastion.public_dns
  description = "Bastion host public DNS name"
}

output "vpc_id" {
  value = aws_vpc.main.id
}

output "vpc_cidr_block" {
  value = aws_vpc.main.cidr_block
}

output "private_db_subnets_id" {
  value = aws_subnet.private_db[*].id
}

output "private_ecs_subnets_id" {
  value = aws_subnet.private_db[*].id
}

output "public_subnets_id" {
  value = aws_subnet.public[*].id
}