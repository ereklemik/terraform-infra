resource "aws_subnet" "private_ecs" {
  count = length(var.private_ecs_subnets)

  vpc_id            = aws_vpc.main.id
  availability_zone = var.availability_zone_names[count.index % length(var.availability_zone_names)]
  cidr_block        = var.private_ecs_subnets[count.index]

  tags = {
    Name        = "${var.environment_prefix}-subnet-private${count.index + 1}-${var.availability_zone_names[count.index % length(var.availability_zone_names)]}"
    Environment = var.environment
  }
}

resource "aws_subnet" "private_db" {
  count = length(var.private_db_subnets)

  vpc_id            = aws_vpc.main.id
  availability_zone = var.availability_zone_names[count.index % length(var.availability_zone_names)]
  cidr_block        = var.private_db_subnets[count.index]

  tags = {
    Name        = "${var.environment_prefix}-subnet-private${count.index + 1 + length(var.private_ecs_subnets)}-${var.availability_zone_names[count.index % length(var.availability_zone_names)]}"
    Environment = var.environment
  }
}

resource "aws_route_table" "private" {
  count  = length(var.private_ecs_subnets) + length(var.private_db_subnets)
  vpc_id = aws_vpc.main.id

  route {
    cidr_block           = "0.0.0.0/0"
    network_interface_id = aws_instance.nat[count.index % length(var.availability_zone_names)].primary_network_interface_id
  }

  tags = {
    Name        = "${var.environment_prefix}-rtb-private${count.index + 1}-${var.availability_zone_names[count.index % length(var.availability_zone_names)]}"
    Environment = var.environment
  }
  lifecycle {
    ignore_changes = all
  }
}

resource "aws_route_table_association" "private_ecs_association" {
  count = length(var.private_ecs_subnets)

  subnet_id      = aws_subnet.private_ecs[count.index].id
  route_table_id = aws_route_table.private[count.index].id
}

resource "aws_route_table_association" "private_db_association" {
  count = length(var.private_db_subnets)

  subnet_id      = aws_subnet.private_db[count.index].id
  route_table_id = aws_route_table.private[count.index + length(var.private_ecs_subnets)].id
}