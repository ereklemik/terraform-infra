resource "aws_subnet" "public" {
  count = length(var.public_subnets)

  vpc_id            = aws_vpc.main.id
  availability_zone = var.availability_zone_names[count.index % length(var.availability_zone_names)]
  cidr_block        = var.public_subnets[count.index]

  tags = {
    Name        = "${var.environment_prefix}-subnet-public${count.index + 1}-${var.availability_zone_names[count.index % length(var.availability_zone_names)]}"
    Environment = var.environment
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name        = "${var.environment_prefix}-igw"
    Environment = var.environment
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name        = "${var.environment_prefix}-rtb-public"
    Environment = var.environment
  }
  lifecycle {
    ignore_changes = all
  }
}

resource "aws_route_table_association" "public_association" {
  count = length(var.public_subnets)

  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public.id
}