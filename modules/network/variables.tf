variable "availability_zone_names" {
  type = list(string)
}

variable "project_prefix" {
  type = string
}

variable "environment_prefix" {
  type = string
}

variable "environment" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "public_subnets" {
  type = list(string)
}

variable "private_ecs_subnets" {
  type = list(string)
}

variable "private_db_subnets" {
  type = list(string)
}

variable "db_engine" {
  type = string
}

variable "nat_instance_type" {
  type = string
}

variable "bastion_instance_type" {
  type = string
}

variable "key_name" {
  type        = string
  description = "name of ssh key pair"
}