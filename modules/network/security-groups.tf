resource "aws_security_group" "nat" {
  name        = "${var.project_prefix}-${var.environment_prefix}-nat-instance-sg"
  description = "${var.project_prefix}-${var.environment_prefix}-nat-instance-sg"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [aws_vpc.main.cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name        = "${var.project_prefix}-${var.environment_prefix}-nat-instance-sg"
    Environment = var.environment
  }
}

resource "aws_security_group" "bastion" {
  name        = "${var.project_prefix}-${var.environment_prefix}-bastion-instance-mgmt-sg"
  description = "${var.project_prefix}-${var.environment_prefix}-bastion-instance-mgmt-sg"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name        = "${var.project_prefix}-${var.environment_prefix}-bastion-instance-mgmt-sg"
    Environment = var.environment
  }
}