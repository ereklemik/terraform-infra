resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr
  tags = {
    Name        = "${var.environment_prefix}-vpc"
    Environment = var.environment
  }
}