output "s3_domain_name" {
  value = aws_s3_bucket.main.bucket_domain_name
}
output "s3_arn" {
  value = aws_s3_bucket.main.arn
}
output "s3_bucket_name" {
  value = aws_s3_bucket.main.id
}