resource "aws_s3_bucket" "main" {
  bucket = "${var.project_prefix}-${var.environment_prefix}-bucket"
  tags = {
    Name        = "${var.project_prefix}-${var.environment_prefix}"
    Environment = var.environment
  }
}

resource "aws_s3_bucket_cors_configuration" "example" {
  bucket = aws_s3_bucket.main.bucket

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST", "GET", "DELETE"]
    allowed_origins = ["*"]
    expose_headers  = [""]
  }
}