variable "project_prefix" {
  type = string
}

variable "environment_prefix" {
  type = string
}

variable "environment" {
  type = string
}