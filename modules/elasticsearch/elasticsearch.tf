resource "aws_security_group" "es" {
  name        = "${var.project_prefix}-${var.environment_prefix}-os-standalone-sg"
  description = "${var.project_prefix}-${var.environment_prefix}-os-standalone-sg"
  vpc_id      = var.vpc_id

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.project_prefix}-${var.environment_prefix}-os-standalone-sg"
    Environment = var.environment
  }
}

data "aws_ssm_parameter" "password" {
  name = "/${var.project_prefix}/${var.environment_prefix}/ES_PASSWORD"
}


resource "aws_elasticsearch_domain" "es" {
  domain_name           = "${var.project_prefix}-${var.environment_prefix}-os-standalone"
  elasticsearch_version = var.elasticsearch_version

  domain_endpoint_options {
    enforce_https       = var.enforce_https
    tls_security_policy = "Policy-Min-TLS-1-2-2019-07"
  }

  ebs_options {
    ebs_enabled = var.ebs_enabled
    volume_size = var.volume_size
  }

  encrypt_at_rest {
    enabled = var.encrypt_at_rest
  }

  node_to_node_encryption {
    enabled = var.node_to_node_encryption
  }

  cluster_config {
    instance_type  = var.instance_type
    instance_count = 1
  }

  advanced_security_options {
    enabled                        = true
    internal_user_database_enabled = true
    master_user_options {
      master_user_name     = var.username
      master_user_password = data.aws_ssm_parameter.password.value
    }
  }

  vpc_options {
    subnet_ids = [var.subnet_ids]

    security_group_ids = [aws_security_group.es.id]
  }

  tags = {
    Name        = "${var.project_prefix}-${var.environment_prefix}-os-standalone"
    Environment = var.environment
  }

}

resource "aws_elasticsearch_domain_policy" "main" {
  domain_name = aws_elasticsearch_domain.es.domain_name

  access_policies = <<POLICIES
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "es:*",
      "Resource": "${aws_elasticsearch_domain.es.arn}/*"
    }
  ]
}
POLICIES
}