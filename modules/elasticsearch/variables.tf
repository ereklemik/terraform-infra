variable "project_prefix" {
  type = string
}

variable "environment_prefix" {
  type = string
}

variable "environment" {
  type = string
}

variable "vpc_id" {
}

variable "vpc_cidr_block" {
}

variable "subnet_ids" {
}

variable "instance_type" {
  type = string
}

variable "elasticsearch_version" {
  type = string
}

variable "availability_zone" {
  type = string
}

variable "encrypt_at_rest" {
  type    = bool
  default = true
}

variable "node_to_node_encryption" {
  type    = bool
  default = true
}

variable "enforce_https" {
  type    = bool
  default = true
}

variable "ebs_enabled" {
  type    = bool
  default = true
}

variable "volume_size" {
  type    = number
  default = 10
}

variable "username" {
  type = string
}