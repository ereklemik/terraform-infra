resource "aws_elasticache_subnet_group" "default" {
  name       = "${var.project_prefix}-${var.environment_prefix}-redis-standalone-subnetgroup"
  subnet_ids = var.subnet_ids

  tags = {
    Name        = "${var.project_prefix}-${var.environment_prefix}-redis-standalone-subnetgroup"
    Environment = var.environment
  }
}

resource "aws_security_group" "redis" {
  name        = "${var.project_prefix}-${var.environment_prefix}-redis-standalone-cache-sg"
  description = "${var.project_prefix}-${var.environment_prefix}-redis-standalone-cache-sg"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = var.port
    to_port     = var.port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_ssm_parameter" "password" {
  name = "/${var.project_prefix}/${var.environment_prefix}/REDIS_PASSWORD"
}

resource "aws_elasticache_replication_group" "default" {
  automatic_failover_enabled  = false
  preferred_cache_cluster_azs = [var.availability_zone]
  replication_group_id        = "${var.project_prefix}-${var.environment_prefix}-redis-standalone"
  description                 = "${var.project_prefix}-${var.environment_prefix}-redis-standalone"
  at_rest_encryption_enabled  = var.at_rest_encryption_enabled
  transit_encryption_enabled  = var.transit_encryption_enabled
  node_type                   = var.instance_type
  num_cache_clusters          = 1
  multi_az_enabled            = false
  parameter_group_name        = var.parameter_group_name
  engine_version              = var.engine_version
  port                        = var.port
  auth_token                  = data.aws_ssm_parameter.password.value

  security_group_ids = [aws_security_group.redis.id]
  subnet_group_name  = "${var.project_prefix}-${var.environment_prefix}-redis-standalone-subnetgroup"

  tags = {
    Name        = "${var.project_prefix}-${var.environment_prefix}-redis-standalone"
    Environment = var.environment
  }
}