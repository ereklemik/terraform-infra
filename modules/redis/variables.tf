variable "project_prefix" {
  type = string
}

variable "environment_prefix" {
  type = string
}

variable "environment" {
  type = string
}

variable "vpc_id" {
}

variable "vpc_cidr_block" {
}

variable "subnet_ids" {
}

variable "instance_type" {
  type = string
}

variable "engine_version" {
  type = string
}

variable "availability_zone" {
  type = string
}

variable "transit_encryption_enabled" {
  type    = bool
  default = true
}

variable "at_rest_encryption_enabled" {
  type    = bool
  default = true
}

variable "parameter_group_name" {
  type    = string
  default = "default.redis4.0"
}

variable "port" {
  type    = number
  default = 6379
}
