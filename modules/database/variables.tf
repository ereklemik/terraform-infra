variable "project_prefix" {
  type = string
}

variable "environment_prefix" {
  type = string
}

variable "environment" {
  type = string
}

variable "db_instance_type" {
  type = string
}

variable "subnet_ids" {
}

variable "vpc_id" {
}

variable "vpc_cidr_block" {
}

variable "db_engine" {
  type = string
}

variable "db_engine_version" {
  type = string
}

variable "db_storage_size" {
  type = number
}

variable "db_name" {
  type = string
}

variable "db_username" {
  type = string
}

variable "db_availability_zone" {
  type = string
}

variable "db_skip_final_snapshot" {
  type = bool
}
