resource "aws_db_subnet_group" "default" {

  name       = "${var.project_prefix}-${var.environment_prefix}-${var.db_engine}-standalone-subnetgroup"
  subnet_ids = var.subnet_ids

  tags = {
    Name        = "${var.project_prefix}-${var.environment_prefix}-${var.db_engine}-standalone-subnetgroup"
    Environment = var.environment
  }
}

resource "aws_security_group" "mysql" {
  name        = "${var.project_prefix}-${var.environment_prefix}-${var.db_engine}-standalone-db-sg"
  description = "${var.project_prefix}-${var.environment_prefix}-${var.db_engine}-standalone-db-sg"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.project_prefix}-${var.environment_prefix}-${var.db_engine}-standalone-db-sg"
    Environment = var.environment
  }
}

data "aws_ssm_parameter" "password" {
  name = "/${var.project_prefix}/${var.environment_prefix}/DB_PASSWORD"
}

resource "aws_db_instance" "default" {
  identifier          = "${var.project_prefix}-${var.environment_prefix}-${var.db_engine}-standalone"
  allocated_storage   = var.db_storage_size
  engine              = var.db_engine
  engine_version      = var.db_engine_version
  instance_class      = var.db_instance_type
  db_name             = var.db_name
  username            = var.db_username
  password            = data.aws_ssm_parameter.password.value
  skip_final_snapshot = var.db_skip_final_snapshot
  availability_zone   = var.db_availability_zone

  vpc_security_group_ids = [aws_security_group.mysql.id]
  db_subnet_group_name   = "${var.project_prefix}-${var.environment_prefix}-${var.db_engine}-standalone-subnetgroup"

  tags = {
    Name        = "${var.project_prefix}-${var.environment_prefix}-${var.db_engine}-standalone"
    Environment = var.environment
  }
  lifecycle {
    ignore_changes = all
  }
}