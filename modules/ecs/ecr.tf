resource "aws_ecr_repository" "main" {
  name                 = "${var.project_prefix}-${var.environment_prefix}-laravel"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }

  tags = {
    Environment = var.environment
  }
}