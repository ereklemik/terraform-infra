resource "aws_iam_policy" "task_execution_policy" {
  name = "laravel-${var.environment_prefix}-ecs-task-policy"

  policy = jsonencode({
    "Statement" : [
      {
        "Action" : [
          "s3:*",
          "s3:ListBucket"
        ],
        "Effect" : "Allow",
        "Resource" : [
          "arn:aws:s3:::wiv-${var.environment_prefix}-bucket/*",
          "arn:aws:s3:::wiv-${var.environment_prefix}-bucket"
        ]
      },
      {
        "Action" : [
          "ssm:GetParametersByPath",
          "ssm:GetParameters",
          "ssm:DescribeParameters",
          "kms:Decrypt"
        ],
        "Effect" : "Allow",
        "Resource" : [
          "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}*",
          "arn:aws:kms:${var.region}:${var.account_id}:alias/ssm"
        ]
      }
    ],
    "Version" : "2012-10-17"
  })
}

resource "aws_iam_role" "task_execution_role" {
  name = "laravel-${var.environment_prefix}-iam-ecs-task-execution-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    Environment = var.environment
  }
}

resource "aws_iam_role" "task_role" {
  name = "laravel-${var.environment_prefix}-iam-ecs-task-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    Environment = var.environment
  }
}

resource "aws_iam_role_policy_attachment" "task_execution_policy_attach" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_policy.arn
}

resource "aws_iam_role_policy_attachment" "task_policy_attach" {
  role       = aws_iam_role.task_role.name
  policy_arn = aws_iam_policy.task_execution_policy.arn
}

resource "aws_iam_role_policy_attachment" "amazon_task_execution_role_attach" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "amazon_task_role_attach" {
  role       = aws_iam_role.task_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}
