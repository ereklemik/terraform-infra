resource "aws_security_group" "ecs_laravel_api" {
  name        = "${var.project_prefix}-${var.environment_prefix}-laravel-api-ecs-service-sg"
  description = "${var.project_prefix}-${var.environment_prefix}-laravel-api-ecs-service-sg"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.project_prefix}-${var.environment_prefix}-laravel-api-ecs-service-sg"
    Environment = var.environment
  }
}
