resource "aws_ecs_task_definition" "laravel_api_task_definition" {
  family                   = "laravel-api-${var.environment_prefix}"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 2048
  memory                   = 4096
  task_role_arn            = aws_iam_role.task_role.arn
  execution_role_arn       = aws_iam_role.task_execution_role.arn
  container_definitions = jsonencode([
    {
      name      = "laravel"
      image     = "${var.laravel_image}:latest"
      essential = true
      portMappings = [
        {
          containerPort = var.laravel_port
          hostPort      = var.laravel_port
        }
      ]
      logConfiguration = {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-region" : "${var.region}",
          "awslogs-group" : "${aws_cloudwatch_log_group.laravel.name}",
          "awslogs-stream-prefix" : "ecs"
        }
      }
      environment = [
        { "name" : "AWS_DEFAULT_REGION", "value" : "eu-central-1" },
        { "name" : "BROADCAST_DRIVER", "value" : "pusher" },
        { "name" : "DB_CONNECTION", "value" : "mysql" },
        { "name" : "DB_PORT", "value" : "3306" },
        { "name" : "FILESYSTEM_DRIVER", "value" : "s3" },
        { "name" : "LOG_CHANNEL", "value" : "stderr" },
        { "name" : "MAIL_ADDRESS", "value" : "feedback@mail.wiv.club" },
        { "name" : "MAIL_DRIVER", "value" : "mailgun" },
        { "name" : "MAILGUN_DOMAIN", "value" : "mail.wiv.club" },
        { "name" : "CACHE_DRIVER", "value" : "redis" },
        { "name" : "QUEUE_CONNECTION", "value" : "redis" },
        { "name" : "REDIS_CACHE_DB", "value" : "1" },
        { "name" : "REDIS_DB", "value" : "0" },
        { "name" : "REDIS_CLIENT", "value" : "predis" },
        { "name" : "REDIS_PORT", "value" : "6379" },
        { "name" : "REDIS_SCHEME", "value" : "tls" },
        { "name" : "SESSION_DRIVER", "value" : "redis" }
      ]
      secrets = [
        { "name" : "CHAT_API_KEY", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/CHAT_API_KEY" },
        { "name" : "CHAT_APP_ID", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/CHAT_APP_ID" },
        { "name" : "AWS_BUCKET", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/AWS_BUCKET" },
        { "name" : "APP_URL", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/APP_URL" },
        { "name" : "APP_ENV", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/APP_ENV" },
        { "name" : "APP_DEBUG", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/APP_DEBUG" },
        { "name" : "BS_URL", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/BS_URL" },
        { "name" : "BS_PASSWORD", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/BS_PASSWORD" },
        { "name" : "BS_USER", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/BS_USER" },
        { "name" : "DB_HOST", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/DB_HOST" },
        { "name" : "DB_DATABASE", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/DB_DATABASE" },
        { "name" : "DB_USERNAME", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/DB_USERNAME" },
        { "name" : "DB_PASSWORD", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/DB_PASSWORD" },
        { "name" : "HUBSPOT_API_KEY", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/HUBSPOT_API_KEY" },
        { "name" : "JWT_SECRET", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/JWT_SECRET" },
        { "name" : "MAILGUN_SECRET", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/MAILGUN_SECRET" },
        { "name" : "PASSPORT_PRIVATE_KEY", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PASSPORT_PRIVATE_KEY" },
        { "name" : "PASSPORT_PUBLIC_KEY", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PASSPORT_PUBLIC_KEY" },
        { "name" : "PUSHER_APP_CLUSTER", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PUSHER_APP_CLUSTER" },
        { "name" : "PUSHER_APP_ID", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PUSHER_APP_ID" },
        { "name" : "PUSHER_APP_KEY", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PUSHER_APP_KEY" },
        { "name" : "PUSHER_APP_SECRET", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PUSHER_APP_SECRET" },
        { "name" : "PUSHER_BEAM_ID", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PUSHER_BEAM_ID" },
        { "name" : "PUSHER_BEAM_KEY", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PUSHER_BEAM_KEY" },
        { "name" : "MIX_PUSHER_APP_CLUSTER", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/MIX_PUSHER_APP_CLUSTER" },
        { "name" : "MIX_PUSHER_APP_KEY", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/MIX_PUSHER_APP_KEY" },
        { "name" : "ES_PREFIX", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/ES_PREFIX" },
        { "name" : "ES_HOST", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/ES_HOST" },
        { "name" : "REDIS_HOST", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/REDIS_HOST" },
        { "name" : "REDIS_PASSWORD", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/REDIS_PASSWORD" },
      ]

    },
    {
      name      = "nginx"
      image     = "${var.nginx_image}:latest"
      essential = true
      logConfiguration = {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-region" : "${var.region}",
          "awslogs-group" : "${aws_cloudwatch_log_group.nginx.name}",
          "awslogs-stream-prefix" : "ecs"
        }
      }
      portMappings = [
        {
          containerPort = var.nginx_port
          hostPort      = var.nginx_port
        }
      ]
    }
  ])

  tags = {
    Environment = var.environment
  }
}

resource "aws_ecs_service" "laravel_api_service" {
  name            = "laravel-api"
  cluster         = aws_ecs_cluster.laravel_api_cluster.id
  desired_count   = 1
  launch_type     = "FARGATE"
  task_definition = aws_ecs_task_definition.laravel_api_task_definition.arn

  deployment_minimum_healthy_percent = 100
  deployment_maximum_percent         = 200


  network_configuration {
    subnets         = var.subnet_ids
    security_groups = [aws_security_group.ecs_laravel_api.id]
  }
  load_balancer {
    target_group_arn = var.alb_target_group_arn
    container_name   = "nginx"
    container_port   = var.nginx_port
  }

  lifecycle {
    ignore_changes = [desired_count]
  }

  tags = {
    Environment = var.environment
  }
}

resource "aws_appautoscaling_target" "ecs_target" {
  max_capacity       = 4
  min_capacity       = 1
  resource_id        = "service/${var.project_prefix}-${var.environment_prefix}-cluster/laravel-api"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "ecs_cpu_policy" {
  name               = "cpu-scale"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
    target_value       = 30
    scale_in_cooldown  = 300
    scale_out_cooldown = 30
  }
}

resource "aws_appautoscaling_policy" "ecs_ram_policy" {
  name               = "memory-scale"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }
    target_value       = 60
    scale_in_cooldown  = 300
    scale_out_cooldown = 30
  }
}

resource "aws_ecs_task_definition" "worker_task_definition" {
  family                   = "laravel-worker-${var.environment_prefix}"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  task_role_arn            = aws_iam_role.task_role.arn
  execution_role_arn       = aws_iam_role.task_execution_role.arn
  container_definitions = jsonencode([
    {
      name      = "laravel"
      image     = "${var.laravel_image}:latest"
      essential = true
      portMappings = [
        {
          containerPort = var.laravel_port
          hostPort      = var.laravel_port
        }
      ]
      entryPoint = [
        "/usr/local/bin/php",
        "artisan",
        "horizon"
      ],
      logConfiguration = {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-region" : "${var.region}",
          "awslogs-group" : "${aws_cloudwatch_log_group.laravel.name}",
          "awslogs-stream-prefix" : "ecs"
        }
      }
      environment = [
        { "name" : "AWS_DEFAULT_REGION", "value" : "eu-central-1" },
        { "name" : "BROADCAST_DRIVER", "value" : "pusher" },
        { "name" : "DB_CONNECTION", "value" : "mysql" },
        { "name" : "DB_PORT", "value" : "3306" },
        { "name" : "FILESYSTEM_DRIVER", "value" : "s3" },
        { "name" : "LOG_CHANNEL", "value" : "stderr" },
        { "name" : "MAIL_ADDRESS", "value" : "feedback@mail.wiv.club" },
        { "name" : "MAIL_DRIVER", "value" : "mailgun" },
        { "name" : "MAILGUN_DOMAIN", "value" : "mail.wiv.club" },
        { "name" : "CACHE_DRIVER", "value" : "redis" },
        { "name" : "QUEUE_CONNECTION", "value" : "redis" },
        { "name" : "REDIS_CACHE_DB", "value" : "1" },
        { "name" : "REDIS_DB", "value" : "0" },
        { "name" : "REDIS_CLIENT", "value" : "predis" },
        { "name" : "REDIS_PORT", "value" : "6379" },
        { "name" : "REDIS_SCHEME", "value" : "tls" },
        { "name" : "SESSION_DRIVER", "value" : "redis" }
      ]
      secrets = [
        { "name" : "CHAT_API_KEY", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/CHAT_API_KEY" },
        { "name" : "CHAT_APP_ID", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/CHAT_APP_ID" },
        { "name" : "AWS_BUCKET", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/AWS_BUCKET" },
        { "name" : "APP_URL", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/APP_URL" },
        { "name" : "WEB_URL", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/WEB_URL" },
        { "name" : "SHARE_URL", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/SHARE_URL" },
        { "name" : "APP_ENV", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/APP_ENV" },
        { "name" : "APP_DEBUG", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/APP_DEBUG" },
        { "name" : "BS_URL", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/BS_URL" },
        { "name" : "BS_PASSWORD", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/BS_PASSWORD" },
        { "name" : "BS_USER", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/BS_USER" },
        { "name" : "DB_HOST", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/DB_HOST" },
        { "name" : "DB_DATABASE", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/DB_DATABASE" },
        { "name" : "DB_USERNAME", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/DB_USERNAME" },
        { "name" : "DB_PASSWORD", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/DB_PASSWORD" },
        { "name" : "HUBSPOT_API_KEY", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/HUBSPOT_API_KEY" },
        { "name" : "JWT_SECRET", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/JWT_SECRET" },
        { "name" : "MAILGUN_SECRET", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/MAILGUN_SECRET" },
        { "name" : "PASSPORT_PRIVATE_KEY", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PASSPORT_PRIVATE_KEY" },
        { "name" : "PASSPORT_PUBLIC_KEY", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PASSPORT_PUBLIC_KEY" },
        { "name" : "PUSHER_APP_CLUSTER", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PUSHER_APP_CLUSTER" },
        { "name" : "PUSHER_APP_ID", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PUSHER_APP_ID" },
        { "name" : "PUSHER_APP_KEY", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PUSHER_APP_KEY" },
        { "name" : "PUSHER_APP_SECRET", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PUSHER_APP_SECRET" },
        { "name" : "PUSHER_BEAM_ID", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PUSHER_BEAM_ID" },
        { "name" : "PUSHER_BEAM_KEY", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/PUSHER_BEAM_KEY" },
        { "name" : "MIX_PUSHER_APP_CLUSTER", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/MIX_PUSHER_APP_CLUSTER" },
        { "name" : "MIX_PUSHER_APP_KEY", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/MIX_PUSHER_APP_KEY" },
        { "name" : "ES_PREFIX", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/ES_PREFIX" },
        { "name" : "ES_HOST", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/ES_HOST" },
        { "name" : "REDIS_HOST", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/REDIS_HOST" },
        { "name" : "REDIS_PASSWORD", "valueFrom" : "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.project_prefix}/${var.environment_prefix}/REDIS_PASSWORD" },
      ]

    }
  ])

  tags = {
    Environment = var.environment
  }
}

resource "aws_ecs_cluster" "laravel_api_cluster" {
  name = "${var.project_prefix}-${var.environment_prefix}-cluster"

  tags = {
    Environment = var.environment
  }
}
resource "aws_ecs_service" "laravel_worker_service" {
  name            = "laravel-worker"
  cluster         = aws_ecs_cluster.laravel_api_cluster.id
  desired_count   = 1
  launch_type     = "FARGATE"
  task_definition = aws_ecs_task_definition.worker_task_definition.arn


  deployment_minimum_healthy_percent = 100
  deployment_maximum_percent         = 200

  network_configuration {
    subnets         = var.subnet_ids
    security_groups = [aws_security_group.ecs_laravel_api.id]
  }

  lifecycle {
    ignore_changes = [desired_count]
  }

  tags = {
    Environment = var.environment
  }
}

resource "aws_appautoscaling_target" "ecs_worker_target" {
  max_capacity       = 4
  min_capacity       = 1
  resource_id        = "service/${var.project_prefix}-${var.environment_prefix}-cluster/laravel-worker"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "ecs_worker_cpu_policy" {
  name               = "cpu-scale"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_worker_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_worker_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_worker_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
    target_value       = 30
    scale_in_cooldown  = 300
    scale_out_cooldown = 30
  }
}

resource "aws_appautoscaling_policy" "ecs_worker_ram_policy" {
  name               = "memory-scale"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_worker_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_worker_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_worker_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }
    target_value       = 70
    scale_in_cooldown  = 300
    scale_out_cooldown = 30
  }
}