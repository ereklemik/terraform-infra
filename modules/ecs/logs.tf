resource "aws_cloudwatch_log_group" "laravel" {
  name = "${var.project_prefix}-${var.environment_prefix}-laravel-log-group"

  tags = {
    Environment = var.environment
  }
}

resource "aws_cloudwatch_log_group" "nginx" {
  name = "${var.project_prefix}-${var.environment_prefix}-nginx-log-group"

  tags = {
    Environment = var.environment
  }
}

resource "aws_cloudwatch_log_group" "worker" {
  name = "${var.project_prefix}-${var.environment_prefix}-worker-log-group"

  tags = {
    Environment = var.environment
  }
}