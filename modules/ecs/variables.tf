variable "project_prefix" {
  type = string
}

variable "environment_prefix" {
  type = string
}

variable "environment" {
  type = string
}

variable "domain_name" {
  type = string
}

variable "region" {
  type = string
}

variable "account_id" {
  type    = string
  default = "019115857347"
}

variable "vpc_id" {
}

variable "subnet_ids" {
}

variable "laravel_image" {
  type = string
}

variable "laravel_port" {
  type    = number
  default = 9000
}

variable "nginx_image" {
  type = string
}

variable "nginx_port" {
  type    = number
  default = 80
}

variable "alb_target_group_arn" {
  type = string
}

variable "s3_arn" {
  type = string
}