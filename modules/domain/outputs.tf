output "certificate_arn" {
  value = aws_acm_certificate.api_domain_cert.arn
}

output "zone_arn" {
  value = aws_route53_zone.main.arn
}

output "zone_id" {
  value = aws_route53_zone.main.zone_id
}