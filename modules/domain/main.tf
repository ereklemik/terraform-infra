resource "aws_acm_certificate" "api_domain_cert" {
  domain_name       = "api.${var.domain_name}"
  validation_method = "DNS"

  tags = {
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_zone" "main" {
  name = var.domain_name

  tags = {
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "api_domain_validation_record" {
  for_each = {
    for dvo in aws_acm_certificate.api_domain_cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }
  allow_overwrite = true

  name    = each.value.name
  records = [each.value.record]
  ttl     = 60
  type    = each.value.type
  zone_id = aws_route53_zone.main.zone_id

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_acm_certificate_validation" "api_domain_validation" {
  certificate_arn         = aws_acm_certificate.api_domain_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.api_domain_validation_record : record.fqdn]

  lifecycle {
    prevent_destroy = true
  }
}