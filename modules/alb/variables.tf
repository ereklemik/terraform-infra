variable "project_prefix" {
  type = string
}

variable "environment_prefix" {
  type = string
}

variable "environment" {
  type = string
}

variable "domain_name" {
  type = string
}
/*
variable "domain_zone_id" {
  type = string
}
*/
variable "region" {
  type = string
}

variable "public_subnet_ids" {
}

variable "vpc_id" {
}

variable "certificate_arn" {
  type = string
}
