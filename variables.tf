variable "region" {
  type    = string
  default = "eu-central-1"
}

variable "account_id" {
  type    = string
  default = "019115857347"
}

variable "availability_zone_names" {
  type = map(list(string))
  default = {
    "dev"   = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
    "stage" = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
    "prod"  = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
    "qa"    = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]    
  }
}

variable "project_prefix" {
  type    = string
  default = "wiv"
}

variable "environment" {
  type = map(string)
  default = {
    "qa" = "qa"
  }
}

variable "domain_name" {
  type = map(string)
  default = {
    "dev"   = "d.wiv.club"
    "stage" = "s.wiv.club"
    "prod"  = "p.wiv.club"
    "qa"    = "q.wiv.club"

  }
}

variable "vpc_cidr" {
  type    = string
  default = "10.0.0.0/16"
}

variable "public_subnets" {
  type    = list(string)
  default = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24"]
}

variable "private_ecs_subnets" {
  type    = list(string)
  default = ["10.0.16.0/20", "10.0.32.0/20", "10.0.48.0/20"]
}

variable "private_db_subnets" {
  type    = list(string)
  default = ["10.0.64.0/20", "10.0.80.0/20", "10.0.96.0/20"]
}

variable "key_name" {
  type = map(string)
  default = {
    "dev"   = "wiv-devops-ec2"
    "stage" = "wiv-devops-ec2"
    "prod"  = "wiv-devops-ec2"
    "qa"    = "wiv-devops-ec2"    
  }
  description = "name of ssh key pair"
}

variable "nat_instance_type" {
  type = map(string)
  default = {
    "dev"   = "t3.nano"
    "stage" = "t3.nano"
    "prod"  = "t3.nano"
    "qa"    = "t3.nano"
  }
}

variable "bastion_instance_type" {
  type = map(string)
  default = {
    "dev"   = "t3.nano"
    "stage" = "t3.nano"
    "prod"  = "t3.nano"
    "qa"    = "t3.nano"
  }
}

variable "db_instance_type" {
  type = map(string)
  default = {
    "dev"   = "db.t3.medium"
    "stage" = "db.t3.medium"
    "prod"  = "db.t3.medium"
    "qa"    = "db.t3.medium"
  }
}

variable "db_engine" {
  type    = string
  default = "mysql"
}

variable "db_engine_version" {
  type    = string
  default = "5.7.37"
}

variable "db_storage_size" {
  type = map(number)
  default = {
    "dev"   = 20
    "stage" = 20
    "prod"  = 20
    "qa"    = 20
  }
}

variable "db_name" {
  type    = string
  default = "wiv"
}

variable "db_username" {
  type    = string
  default = "admin"
}

variable "db_availability_zone" {
  type = map(string)
  default = {
    "dev"   = "eu-central-1a"
    "stage" = "eu-central-1a"
    "prod"  = "eu-central-1a"
    "qa"    = "eu-central-1a"
  }
}

variable "db_skip_final_snapshot" {
  type    = bool
  default = true
}

variable "redis_instance_type" {
  type = map(string)
  default = {
    "dev"   = "cache.t3.micro"
    "stage" = "cache.t3.micro"
    "prod"  = "cache.t3.micro"
    "qa"    = "cache.t3.micro"
  }
}

variable "redis_engine_version" {
  type    = string
  default = "4.0.10"
}

variable "redis_username" {
  type    = string
  default = "elastic"
}

variable "elasticsearch_instance_type" {
  type = map(string)
  default = {
    "dev"   = "t3.medium.elasticsearch"
    "stage" = "t3.medium.elasticsearch"
    "prod"  = "t3.medium.elasticsearch"
    "qa"    = "t3.medium.elasticsearch"
  }
}

variable "elasticsearch_version" {
  type    = string
  default = "OpenSearch_1.1"
}

variable "elasticsearch_storage_size" {
  type = map(number)
  default = {
    "dev"   = 10
    "stage" = 10
    "prod"  = 10
    "qa"    = 10
  }
}
